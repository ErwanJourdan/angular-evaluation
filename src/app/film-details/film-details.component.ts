import { Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Note } from '../shared/interfaces/note';
import { ApiService } from '../shared/services/api.service';

@Component({
  selector: 'app-film-details',
  templateUrl: './film-details.component.html',
  styleUrls: ['./film-details.component.scss']
})
export class FilmDetailsComponent implements OnInit {


  @Input() film: Note= {
    id: 0,
    title: '',
    year: 0,
    runtime: 0,
    director: '',
    actors: '',
    plot: '',
    posterUrl: '',
    score: 0
  };



  filmId: number = 0;

  constructor(private api:ApiService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.filmId = Number(this.route.snapshot.paramMap.get('id'));
    this.api.getFilmID(this.filmId).subscribe(result => {
      this.film = result;
      return this.film;
    });
  }

}

