import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilmListeComponent } from './film-liste/film-liste.component';
import { HomeComponent } from './home/home.component';
import { FilmDetailsComponent } from './film-details/film-details.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'films', component: FilmListeComponent},
  {path: 'détails/:id', component: FilmDetailsComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
