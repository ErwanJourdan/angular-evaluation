import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeadernavbarComponent } from './headernavbar/headernavbar.component';
import { FilmListeComponent } from './film-liste/film-liste.component';
import { FilmItemComponent } from './film-item/film-item.component';
import { HomeComponent } from './home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FilmDetailsComponent } from './film-details/film-details.component';
import { ScoreButtonComponent } from './score-button/score-button.component';
import { HomeItemComponent } from './home-item/home-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeadernavbarComponent,
    FilmListeComponent,
    FilmItemComponent,
    HomeComponent,
    FilmDetailsComponent,
    ScoreButtonComponent,
    HomeItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
