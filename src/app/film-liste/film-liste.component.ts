import { Component, OnInit } from '@angular/core';
import { Note } from '../shared/interfaces/note';
import { ApiService } from '../shared/services/api.service';

@Component({
  selector: 'app-film-liste',
  templateUrl: './film-liste.component.html',
  styleUrls: ['./film-liste.component.scss']
})
export class FilmListeComponent implements OnInit {

  titre: string = "Liste des films";
  films: Note[]= [];

  constructor(private api:ApiService) { }

  ngOnInit(): void {
    this.api.getFilms().subscribe(getTable => {
      this.films= getTable;
    });
  }
}
