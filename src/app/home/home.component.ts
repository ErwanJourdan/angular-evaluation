import { Component, Input, OnInit } from '@angular/core';
import { Note } from '../shared/interfaces/note';
import { ApiService } from '../shared/services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  films: Note[]= [];

  @Input() film: Note= {
    id: 0,
    title: '',
    year: 0,
    runtime: 0,
    director: '',
    actors: '',
    plot: '',
    posterUrl: '',
    score: 0
  };

  constructor(private api:ApiService) { }

  ngOnInit(): void {
    this.api.getFilms().subscribe(getTable => {
      this.films= getTable;
    });
  }
}
