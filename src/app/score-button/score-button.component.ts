import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Note } from '../shared/interfaces/note';
import { ApiService } from '../shared/services/api.service';

@Component({
  selector: 'app-score-button',
  templateUrl: './score-button.component.html',
  styleUrls: ['./score-button.component.scss']
})
export class ScoreButtonComponent implements OnInit {

  @Input() film: Note= {
    id: 0,
    title: '',
    year: 0,
    runtime: 0,
    director: '',
    actors: '',
    plot: '',
    posterUrl: '',
    score: 0
  };

  filmScore: number = 0;

  @Output() editScore= new EventEmitter<Note>();


  newScore: Note= {
    id: 0,
    title: '',
    year: 0,
    runtime: 0,
    director: '',
    actors: '',
    plot: '',
    posterUrl: '',
    score: 0
  } ;


  constructor(private api:ApiService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    this.filmScore = Number(this.route.snapshot.paramMap.get('id'));
    this.api.getFilmID(this.filmScore).subscribe(result => {
      this.film = result;
      return this.film;
    });
  }



  decreasePoint(scoreToDecrease:number){
    this.newScore = {
      id: this.film.id,
      title: this.film.title,
      year: this.film.year,
      runtime: this.film.runtime,
      director: this.film.director,
      actors: this.film.actors,
      plot: this.film.plot,
      posterUrl: this.film.posterUrl,
      score: this.film.score--
    }
    this.editScore.emit(this.newScore);
  }

  increasePoint(scoreToIncrease: number){
    this.newScore = {
      id: this.film.id,
      title: this.film.title,
      year: this.film.year,
      runtime: this.film.runtime,
      director: this.film.director,
      actors: this.film.actors,
      plot: this.film.plot,
      posterUrl: this.film.posterUrl,
      score: this.film.score++
    }
    this.editScore.emit(this.newScore);
  }

  saveEditScore(editScore: Note) {
      this.api.saveEditScore(editScore).subscribe(saveEditScore => {
        this.film.score
      });
  }
}




