import { Component, Input, OnInit } from '@angular/core';
import { Note } from '../shared/interfaces/note';

@Component({
  selector: 'app-home-item',
  templateUrl: './home-item.component.html',
  styleUrls: ['./home-item.component.scss']
})
export class HomeItemComponent implements OnInit {

  @Input() film: Note= {
    id: 0,
    title: '',
    year: 0,
    runtime: 0,
    director: '',
    actors: '',
    plot: '',
    posterUrl: '',
    score: 0
  };

  constructor() { }

  ngOnInit(): void {
  }

}
