import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Note } from '../shared/interfaces/note';
import { ApiService } from '../shared/services/api.service';

@Component({
  selector: 'app-film-item',
  templateUrl: './film-item.component.html',
  styleUrls: ['./film-item.component.scss']
})

export class FilmItemComponent implements OnInit {

  @Input() film: Note= {
    id: 0,
    title: '',
    year: 0,
    runtime: 0,
    director: '',
    actors: '',
    plot: '',
    posterUrl: '',
    score: 0
  };


  @Output() editScore= new EventEmitter<Note>();


  newScore: Note | undefined;

  constructor(private api:ApiService) { }

  ngOnInit(): void {  }

  decreasePoint(scoreToDecrease:number){
    this.newScore = {
      id: this.film.id,
      title: this.film.title,
      year: this.film.year,
      runtime: this.film.runtime,
      director: this.film.director,
      actors: this.film.actors,
      plot: this.film.plot,
      posterUrl: this.film.posterUrl,
      score: this.film.score--
    }
    this.editScore.emit(this.newScore);
  }

  increasePoint(scoreToIncrease: number){
    this.newScore = {
      id: this.film.id,
      title: this.film.title,
      year: this.film.year,
      runtime: this.film.runtime,
      director: this.film.director,
      actors: this.film.actors,
      plot: this.film.plot,
      posterUrl: this.film.posterUrl,
      score: this.film.score++
    }
    this.editScore.emit(this.newScore);
  }


  saveEditScore(editScore: Note) {
    this.api.saveEditScore(editScore).subscribe(saveEditScore => {
      this.film.score
    });
  }
}
