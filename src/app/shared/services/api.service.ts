import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Note } from '../interfaces/note';

const httpOptions = {
  headers: new HttpHeaders ({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private httpClient: HttpClient) { }

  getFilms(): Observable<Note[]> {
    return this.httpClient.get<Note[]>(`${environment.serverURL}/movies`);

  }

  getFilmID(filmId : number): Observable<Note> {
    return this.httpClient.get<Note>(`${environment.serverURL}/movies/${filmId}`, httpOptions);

  }

  getFilmScore(filmScore : number): Observable<Note> {
    return this.httpClient.get<Note>(`${environment.serverURL}/movies/${filmScore}`, httpOptions);

  }

  saveEditScore(film: Note){
    return this.httpClient.put<Note>(`${environment.serverURL}/movies/${film.id}`, film, httpOptions);
  }








}


