import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import {filter} from "rxjs/operators";

@Component({
  selector: 'app-headernavbar',
  templateUrl: './headernavbar.component.html',
  styleUrls: ['./headernavbar.component.scss']
})
export class HeadernavbarComponent implements OnInit {


  currentUrl = ''

  constructor(private router: Router) {
    router.events
      .pipe(
        filter( event => event instanceof NavigationEnd)
      )
      .subscribe( event =>
      {
        this.currentUrl = router.url
      });
   }

  ngOnInit(): void {

  }

}

